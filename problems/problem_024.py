# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
list1 = []
def calculate_average(values):
    sums = 0
    if len(values)== 0:
        return None
    for i in values:
        sums = i + sums
    avg = sums / len(values)
    return avg


print(calculate_average(list1))
