# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
list1 = [5,6,7,8,9,19]
def calculate_sum(values):
    sums = 0
    if len(values) == 0:
        return None
    if len(values) > 0:
        for i in values:
            sums = i + sums
        return sums
print(calculate_sum(list1))
