# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
lis1 = [1,2,3]
def find_second_largest(values):
    mval = 0
    # secval = 0
    for i in values:
        if mval < i:
            mval = i
    return mval
print(find_second_largest(lis1))
