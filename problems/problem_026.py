# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average
list1 = [90,10,50,80,90]
def calculate_grade(values):
    sums = 0
    avg = 0
    for i in values:
        sums = i + sums
    avg = sums / len(values)
    if avg >= 90:
        return "A A Avg"
    elif avg >= 80 and avg < 90:
        return "A B Avg"
    elif avg >= 70 and avg < 80:
        return "A C Avg"
    elif avg >= 60 and avg < 70:
        return "A D Avg"
    else:
        return"F"
print(calculate_grade(list1))
