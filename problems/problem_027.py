# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#
list1 = [0,0,0,0,0,1]
def max_in_list(values):
    maxval = 0
    if len(values) == 0:
        return None
    for i in values:
        if maxval < i:
            maxval = i
    return maxval
print(max_in_list(list1))
